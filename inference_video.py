import sys
import moviepy.editor as mpy
import pims
import numpy as np
import torch as tr
from neural_wrappers.models import ModelLaina, ModelUNetDilatedConv
from neural_wrappers.pytorch import maybeCuda
from neural_wrappers.readers.cityscapes_reader import cityscapesSemanticCmap
from datetime import datetime
from Mihlib import show_plots, plot_image
from functools import partial
from lycon import resize, Interpolation
from matplotlib.cm import hot

from inference import normalize, getInputMetadata, doInference

prevImage = None

def minMaxNormalizeFrame(frame):
	Min, Max = np.min(frame), np.max(frame)
	frame -= Min
	frame /= (Max - Min)
	frame *= 255
	return np.uint8(frame)

def make_frame(t, model, video, fps, inputShape, args):
	global prevImage
	t = min(int(t * fps), len(video) - 1)
	currentFrame = np.array(video[t])
	outH, outW = currentFrame.shape[0 : 2]
	inH, inW = inputShape

	if args.inference_optical_flow and (prevImage is None or t == 0):
		prevImage = currentFrame
		return np.zeros((outH, outW, 3), dtype=np.uint8)

	numChannels, _, dimensionsIndexes = getInputMetadata(args)

	# RGB
	image = resize(currentFrame, height=inH, width=inW, interpolation=Interpolation.CUBIC)
	image = normalize(image, args.data_normalization)
	images = {
		"currentImage" : image, 
	}

	if args.inference_rgb_prev_frame:
		assert not prevImage is None
		images["prevImage"] = prevImage

	if args.inference_rgb_older_frame:
		assert False, "Video with rgb_older_frame (First Frame) -- Not supported"

	if args.inference_prev_depth:
		assert False, "Video with depth prior -- Not yet supported"

	npInput, npResult = doInference(args, model, images, inputShape, numChannels, dimensionsIndexes)
	npResult = minMaxNormalizeFrame(npResult)
	npResult = resize(npResult, height=outH, width=outW, interpolation=Interpolation.CUBIC)
	finalFrame = minMaxNormalizeFrame(hot(npResult)[..., 0 : 3])

	# Update various variables (prevImage, prevDepth etc.)
	prevImage = images["currentImage"]

	return finalFrame

def exportVideo(model, args):
	print("In video: %s. Out video: %s" % (args.inference_input_video, args.inference_output_video))
	video = pims.PyAVReaderIndexed(args.inference_input_video)
	fps = 30
	duration = max(0, int(len(video) / fps) - 2)

	inH, inW = map(lambda x : int(x), args.inference_input_shape.split(","))
	frameCallback = partial(make_frame, model=model, video=video, fps=fps, inputShape=(inH, inW), args=args)
	clip = mpy.VideoClip(frameCallback, duration=duration)
	clip.write_videofile(args.inference_output_video, fps=fps, verbose=False, progress_bar=True)
