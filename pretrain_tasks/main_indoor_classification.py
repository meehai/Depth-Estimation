import sys
from Mihlib import plot_image, show_plots
from neural_wrappers.pytorch import SelfSupervisedNetwork, maybeCuda
from neural_wrappers.readers import IndoorCVPR09Reader
from neural_wrappers.models import ModelUNetDilatedConv, UpSampleLayer
from neural_wrappers.callbacks import SaveModelsSelfSupervised, SaveModels, Callback
from neural_wrappers.metrics import Accuracy
import torch.nn as nn
import torch.optim as optim
import torch as tr
import torch.nn.functional as F

sys.path.append("..")

class ModelClassificationDecoderOnly(SelfSupervisedNetwork):
	def __init__(self, **kwargs):
		super().__init__(ModelUNetDilatedConv(**kwargs))

		self.upConv1 = nn.ConvTranspose2d(in_channels=kwargs["numFilters"] * 8 * 6, out_channels=1024, \
			kernel_size=3, stride=1)
		self.upConv2 = nn.ConvTranspose2d(in_channels=1024, out_channels=256, kernel_size=3, stride=1)
		self.upConv3 = nn.ConvTranspose2d(in_channels=256, out_channels=32, kernel_size=3, stride=1)
		self.upLinear1 = nn.Linear(32 * 38 * 70, 100)
		self.upLinear2 = nn.Linear(100, 67)

	def forward(self, x):
		x = tr.transpose(tr.transpose(x, 1, 3), 2, 3)
		y_down1 = self.baseModel.downBlock1(x)
		y_down1pool = self.baseModel.pool1(y_down1)
		y_down2 = self.baseModel.downBlock2(y_down1pool)
		y_down2pool = self.baseModel.pool2(y_down2)
		y_down3 = self.baseModel.downBlock3(y_down2pool)
		y_down3pool = self.baseModel.pool3(y_down3)

		y_dilate1 = F.relu(self.baseModel.dilate1(y_down3pool))
		y_dilate2 = F.relu(self.baseModel.dilate2(y_dilate1))
		y_dilate3 = F.relu(self.baseModel.dilate3(y_dilate2))
		y_dilate4 = F.relu(self.baseModel.dilate4(y_dilate3))
		y_dilate5 = F.relu(self.baseModel.dilate5(y_dilate4))
		y_dilate6 = F.relu(self.baseModel.dilate6(y_dilate5))
		y_dilate_concatenate = tr.cat([y_dilate1, y_dilate2, y_dilate3, y_dilate4, y_dilate5, y_dilate6], dim=1)

		y_up1 = F.relu(self.upConv1(y_dilate_concatenate))
		y_up2 = F.relu(self.upConv2(y_up1))
		y_up3 = F.relu(self.upConv3(y_up2))
		y_up3 = y_up3.view(-1, 32 * 38 * 70)
		y_up4 = F.relu(self.upLinear1(y_up3))
		y_up5 = F.log_softmax(self.upLinear2(y_up4), dim=1)
		return y_up5

	def pretrainLayersSetup(self):
		if self.pretrain == False:
			self.upConv1 = None
			self.upConv2 = None
			self.upConv3 = None
			self.upLinear1 = None
			self.upLinear2 = None

class PlotCallback(Callback):
	def __init__(self, classes):
		self.classes = classes

	def onIterationEnd(self, **kwargs):
		from Mihlib import plot_image, show_plots

		data = kwargs["data"]
		labels = kwargs["labels"]
		results = kwargs["results"]
		loss = kwargs["loss"]

		for i in range(len(results)):
			image = data[i]
			argMaxResult = results[i].max()
			labelResult = results[i][labels[i]]
			result = self.classes[results[i].argmax()]
			label = self.classes[labels[i]]

			plot_image(image)
			print("Label: %s. Result: %s with value %2.2f. Result of label value %2.2f" % \
				(label, result, argMaxResult, labelResult))
			show_plots()


def main():
	MB = 2
	reader = IndoorCVPR09Reader(sys.argv[2], imageShape=(256, 512, 3))
	generator = reader.iterate("train", miniBatchSize=MB, maxPrefetch=1)
	numSteps = reader.getNumIterations("train", miniBatchSize=MB)
	valGenerator = reader.iterate("validation", miniBatchSize=MB, maxPrefetch=1)
	valNumSteps = reader.getNumIterations("validation", miniBatchSize=MB)
	print(reader.summary())

	# for items in generator:
	# 	images, labels = items
	# 	for j in range(len(images)):
	# 		plot_image(images[j])
	# 		print(reader.getClasses()[labels[j]])
	# 		show_plots()

	model = ModelClassificationDecoderOnly(inputShape=(256, 512, 3), numFilters=64)
	model = maybeCuda(model)
	model.setOptimizer(optim.Adam, lr=0.001)
	model.setCriterion(nn.NLLLoss())
	model.setMetrics({ "Accuracy": Accuracy(False) })
	print(model.summary())

	if sys.argv[1] == "train":
		callbacks = [SaveModelsSelfSupervised("best"), SaveModels("improvements")]
		model.train_generator(generator, stepsPerEpoch=numSteps, numEpochs=10, callbacks=callbacks, \
			validationGenerator=valGenerator, validationSteps=valNumSteps)
	elif sys.argv[1] == "retrain":
		callbacks = [SaveModelsSelfSupervised("best"), SaveModels("improvements")]
		model.load_model(sys.argv[3])
		model.train_generator(generator, stepsPerEpoch=numSteps, numEpochs=10, callbacks=callbacks, \
			validationGenerator=valGenerator, validationSteps=valNumSteps)
	elif sys.argv[1] == "add_validation":
		from add_validation import addValidation
		if len(sys.argv) == 4:
			startModel = os.path.abspath(sys.argv[3])
		else:
			startModel = None

		addValidation(model, reader, "models", MB, startModel)
	elif sys.argv[1] == "test":
		model.load_model(sys.argv[3])
		callbacks = [PlotCallback(reader.getClasses())]
		model.test_generator(valGenerator, valNumSteps, callbacks=callbacks)

if __name__ == "__main__":
	main()