import time
import os, sys, shutil
import numpy as np
import torch.optim as optim
from argparse import ArgumentParser
from datetime import datetime, timedelta
from neural_wrappers.pytorch import maybeCuda
from neural_wrappers.models import ModelEigen, ModelLaina, ModelUNet, ModelUNetDilatedConv
from model_unet_convdilated_bugged import ModelUNetDilatedConvBugged
from neural_wrappers.readers import NYUDepthReader, CitySimReader, CityScapesReader, KITTIReader
from neural_wrappers.callbacks import SaveHistory, SaveModels, Callback
from functools import partial

from loss import l2_loss, l2_log_loss, l2_loss_non_zero, l1_loss
from plotter import nicePlotter, test_reader, PlotCallback, paramsHandler, getInputMetadata
from add_validation import addValidation
from inference import inference
from inference_video import exportVideo

def changeDirectory(args):
	if (args.type =="test" and args.test_save_results is False) or args.type in ("add_validation", "inference", \
		"inference_video"):
		return

	mode = "shred" if args.type in ("train", "train_pretrained") else "normal"

	print("Changing to working directory:", args.dir)
	if os.path.exists(args.dir):
		if mode == "shred":
			shutil.rmtree(args.dir)
			os.makedirs(args.dir)
		else:
			print("Directory", args.dir, "already exist, just going there. Writing may overwrite other files.")
	else:
		print("Directory", args.dir, "does not exist. Creating.")
		os.makedirs(args.dir)
	os.chdir(args.dir)

def absPath(x):
	return os.path.abspath(x) if not x is None else None

def parseArguments():
	dateTimeStr = datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d-%H%M%S")

	parser = ArgumentParser()
	parser.add_argument("type", type=str)
	# Dataset stuff
	parser.add_argument("dataset")
	parser.add_argument("dataset_path", help="Path to dataset")
	parser.add_argument("--dir", default=dateTimeStr)

	# Datast and Data stuff
	parser.add_argument("--data_normalization", default="min_max_normalization", type=str)
	parser.add_argument("--data_transforms", default="none", help="Data transforms. They must be provided as a " + \
		"long string, divided by commas. Example: none,mirror or just none.")
	parser.add_argument("--base_data_group", type=str)
	parser.add_argument("--nyudepth_semantic_num_classes", type=int, default=49)
	# Inference mode stuff. These valuse are also used by datasets that support them for training/testing. For example
	#  inference_rgb_older_frame is a path in inference mode, but can be anything in test mode for CityScapes, and
	#  the test method will look for rgb_first_frame inside CityScapes dataset, and provide necessary data for
	#  training or testing. Same goes for semantic/optical_flow/last_frame.
	parser.add_argument("--inference_rgb", type=str)
	parser.add_argument("--inference_rgb_older_frame", type=str)

	# For RGB prev frame in inference mode, just --inference_rgb_previous_frame is needed.
	# For others (--inference_optical_flow / --inference_semantic), --inference_rgb_previous_frame_path is needed, but
	#  if --inference_rgb_previous_frame is used, that value is taken by default (so no need to specify it).
	parser.add_argument("--inference_rgb_prev_frame")
	parser.add_argument("--inference_rgb_prev_frame_path")
	# For inference mode, --inference_prev_depth is a path to the previous RGB file, for test/train mode, it can
	#  be anything and if not specified it is not used. --inference_prev_depth_mode is the algorithm/network
	#  used to compute the previous depth. For inference mode, this means a network is used to compute the depth
	#  for previous frame, for test mode they are cached in the dataset.
	parser.add_argument("--inference_prev_depth")
	parser.add_argument("--inference_prev_depth_mode")

	parser.add_argument("--inference_optical_flow")
	parser.add_argument("--inference_optical_flow_transform")
	parser.add_argument("--inference_semantic")
	parser.add_argument("--inference_semantic_transform", default="none")
	parser.add_argument("--inference_semantic_num_classes", type=int)

	# Some datasets support horizontal-vertical dimensions ("hvn"). Two modes are supported: "hvn_two_dims" and "none"
	parser.add_argument("--inference_hvn")
	parser.add_argument("--inference_hvn_transform", default="none")

	# For inference mode, we cannot know the input/output shape for all models. As such, we need to define these
	#  variables which take precedence over all other cases. For example, a network trained with UNetConvDiltated
	#  on NYUDeptyV2 has an input shape of (240x320xD), while trained on CityScapes has (256x512xD).
	parser.add_argument("--inference_input_shape")
	parser.add_argument("--inference_output_shape")
	# For inference mode, we may need a secondary model to compute the depth at T-1.
	parser.add_argument("--inference_base_model")
	parser.add_argument("--inference_base_model_weights")
	parser.add_argument("--inference_plot_result", type=int, default=1)

	# For video inference mode
	parser.add_argument("--inference_input_video")
	parser.add_argument("--inference_output_video")

	# Model stuff
	parser.add_argument("--model", help="Supported models: eigen, laina, unet, unet_convdilated")
	parser.add_argument("--eigen_model_part", help="Which parts of the model for eigen (coarse, fine, both).")
	parser.add_argument("--eigen_coarse_weights_file", default=None)
	parser.add_argument("--laina_upsample_method", help="Laina has 3 types of upsampling: unpool, bilinear, " + \
		"nearest, conv_transposed")
	parser.add_argument("--laina_pretrained_model", default=1, type=int)
	parser.add_argument("--unet_initial_filters", default=64, type=int)
	parser.add_argument("--unet_bottleneck_mode", default="dilate2_serial_concatenate")
	parser.add_argument("--weights_file", default=None)

	# Training stuff
	parser.add_argument("--batch_size", default=10, type=int)
	parser.add_argument("--num_epochs", default=100, type=int)
	parser.add_argument("--use_validation_set", default=1, type=int)
	parser.add_argument("--optimizer")
	parser.add_argument("--learning_rate", type=float)
	parser.add_argument("--momentum", default=0.9, type=float)
	parser.add_argument("--loss", default="l2_loss")

	# Other stuff
	parser.add_argument("--test_plot_results", default=0, type=int, help="Plot each result during testing")
	parser.add_argument("--test_save_results", default=0, type=int, help="Save each result during testing")

	# Add validation stuff
	parser.add_argument("--add_validation_models_dir", default=None)
	parser.add_argument("--add_validation_model_start", default=None, help="If a partial validation was done, " + \
		"start from that patch the rest")

	args = parser.parse_args()
	assert args.type in ("train", "test", "retrain", "add_validation", "inference", "test_reader", \
		"train_pretrained", "inference_video")
	assert args.model in ("eigen", "laina", "unet", "unet_convdilated", "unet_convdilated_bugged")
	if args.model == "eigen":
		assert args.eigen_model_part in ("coarse", "fine", "both")
	if args.model == "laina":
		assert args.laina_upsample_method in ("unpool", "bilinear", "nearest", "conv_transposed")
	if args.type in ("inference", "inference_video"):
		assert args.dataset == "none" and args.dataset_path == "none"
	else:
		assert args.dataset in ("nyudepth", "citysim", "cityscapes", "kitti")
	if args.type in ("train", "retrain"):
		assert args.optimizer in ("SGD", "Adam", "RMSProp", "Nesterov")
		assert args.loss in ("l2_loss", "l2_log_loss", "l2_loss_non_zero", "l1_loss")
	assert args.use_validation_set in (0, 1)

	if args.base_data_group is None:
		if args.dataset == "kitti":
			args.base_data_group = "raw_10"
		elif args.dataset == "cityscapes":
			args.base_data_group = "noseq"

	if args.type == "inference":
		if args.inference_rgb_prev_frame:
			args.inference_rgb_prev_frame_path = args.inference_rgb_prev_frame

	args.eigen_coarse_weights_file = absPath(args.eigen_coarse_weights_file)
	args.weights_file = absPath(args.weights_file)
	args.add_validation_models_dir = absPath(args.add_validation_models_dir)
	args.add_validation_model_start = absPath(args.add_validation_model_start)
	args.inference_rgb = absPath(args.inference_rgb)
	args.inference_rgb_older_frame = absPath(args.inference_rgb_older_frame)

	args.use_validation_set = bool(args.use_validation_set)
	args.test_plot_results = bool(args.test_plot_results)
	args.test_save_results = bool(args.test_save_results)
	args.laina_pretrained_model = bool(args.laina_pretrained_model)
	return args

class DurationCallback(Callback):
	def __init__(self):
		self.i = 0
		self.sum = timedelta(0)
		self.first = True

	def onIterationStart(self, **kwargs):
		self.then = datetime.now()

	def onIterationEnd(self, **kwargs):
		now = datetime.now()
		print("Took %s" % (now - self.then))
		if self.first:
			self.first = False
		else:
			self.sum += (now - self.then)
			self.i += 1

	def onEpochEnd(self, **kwargs):
		print("Mean %s" % (self.sum / self.i))

def test(args, model, datasetReader):
	generator = datasetReader.iterate_once("validation", args.batch_size)
	numIterations = datasetReader.getNumIterations("validation", args.batch_size, accountTransforms=True)
	paramsHandlerCallback = partial(paramsHandler, args=args)
	callbacks = [DurationCallback(), \
		PlotCallback(args.test_plot_results, args.test_save_results, paramsHandler=paramsHandlerCallback)]
	finalMetrics = model.test_generator(generator, numIterations, callbacks=callbacks)
	print(finalMetrics)

def setOptimizer(args, model):
	if args.optimizer == "SGD":
		model.setOptimizer(optim.SGD, lr=args.learning_rate, momentum=args.momentum, nesterov=False)
	elif args.optimizer == "Nesterov":
		model.setOptimizer(optim.SGD, lr=args.learning_rate, momentum=args.momentum, nesterov=True)
	elif args.optimizer == "RMSProp":
		model.setOptimizer(optim.RMSProp, lr=args.learning_rate)
	elif args.optimizer == "Adam":
		model.setOptimizer(optim.Adam, lr=args.learning_rate)

def setCriterion(args, model):
	if args.loss == "l2_loss":
		model.setCriterion(l2_loss)
	elif args.loss == "l2_log_loss":
		model.setCriterion(l2_log_loss)
	elif args.loss == "l2_loss_non_zero":
		model.setCriterion(l2_loss_non_zero)
	elif args.loss == "l1_loss":
		model.setCriterion(l1_loss)
	else:
		raise NotImplemented("Wrong loss type. Expected: l2_loss, l2_log_loss, l2_loss_non_zero, l1_loss.")

# Based on the dataset and chose model, we get the shapes of our inputs/outputs that will be used for reader.
def getShapesForModel(args):
	imageShape, depthShape = None, None
	numChannels = getInputMetadata(args)[0]

	if args.type in ("inference", "inference_video"):
		assert args.inference_input_shape
		inH, inW = map(lambda x : int(x), args.inference_input_shape.split(","))
		return (inH, inW, numChannels), None

	if args.model == "laina":
		if args.dataset in ("nyudepth", "citysim"):
			imageShape, depthShape = (228, 304, numChannels), (128, 160)
		elif args.dataset in ("cityscapes", "kitti"):
			imageShape, depthShape = (256, 512, numChannels), (128, 256)
	elif args.model == "eigen":
		if args.dataset in ("nyudepth", "citysim"):
			coarseOnly = args.eigen_model_part == "coarse"
			imageShape = (240, 320, 3)
			depthShape = (55, 74) if coarseOnly else (50, 70)
	elif args.model == "unet":
		if args.dataset in ("nyudepth", "citysim"):
			imageShape, depthShape = (316, 316, 3), (132, 132)
	elif args.model in ("unet_convdilated", "unet_convdilated_bugged"):
		if args.dataset in ("nyudepth", "citysim"):
			imageShape, depthShape = (240, 320, numChannels), (240, 320)
		elif args.dataset in ("cityscapes", "kitti"):
			imageShape, depthShape = (256, 512, numChannels), (256, 512)

	assert (not imageShape is None) and (not depthShape is None)
	return imageShape, depthShape

def getCallbacks(args, datasetReader):
	if args.type in ("train", "train_pretrained"):
		callbacks=[SaveModels(type="all"), SaveHistory("history.txt", mode="write")]
		# When training, also save in history file the summary of the dataset.
		callbacks[1].file.write(datasetReader.summary())
	elif args.type == "retrain":
		callbacks=[SaveModels(type="all"), SaveHistory("history.txt", mode="append")]
	elif args.type in ("test", "add_validation", "inference", "inference_video"):
		callbacks = []
	return callbacks

def getReader(args, imageShape, depthShape, transforms, validationTransforms):
	dataDimensions = getInputMetadata(args)[1]
	if args.dataset == "nyudepth":
		datasetReader = NYUDepthReader(args.dataset_path, imageShape=imageShape, labelShape=depthShape, \
			dataDimensions=dataDimensions, labelDimensions=["depths"], normalization=args.data_normalization, \
			transforms=transforms, dataSplit=(80, 0, 20), semanticNumClasses=args.nyudepth_semantic_num_classes, \
			semanticTransform=args.inference_semantic_transform)
	elif args.dataset == "citysim":
		datasetReader = CitySimReader(args.base_data_group, args.dataset_path, imageShape=imageShape, \
			labelShape=depthShape, transforms=transforms, dataDimensions=dataDimensions, labelDimensions=["depth"], \
			hvnTransform=args.inference_hvn_transform)
	elif args.dataset == "cityscapes":
		resizer = {"rgb" : (*imageShape[0 : 2], 3), "depth" : depthShape[0 : 2]}
		# resizer["depth"] = depthShape[0 : 2]
		datasetReader = CityScapesReader(args.dataset_path, dataDimensions,	\
			resizer=resizer, normalization=args.data_normalization)
		# datasetReader = CityScapesReader(args.dataset_path, imageShape=imageShape, labelShape=depthShape, \
		# 	transforms=["none"], dataDimensions=dataDimensions, baseDataGroup=args.base_data_group,
		# 	normalization=args.data_normalization, semanticTransform=args.inference_semantic_transform, \
		# 	opticalFlowTransform=args.inference_optical_flow_transform)
	elif args.dataset == "kitti":
		datasetReader = KITTIReader(args.dataset_path, imageShape=imageShape, labelShape=depthShape, \
			transforms=["none"], dataDimensions=["rgb"], labelDimensions=["depth"], \
			baseDataGroup=args.base_data_group, normalization=args.data_normalization)
	elif args.dataset == "none":
		datasetReader = None
	print(datasetReader.summary()) if args.dataset != "none" else []

	# Generator stuff (needed just for training)
	trainGenerator, trainSteps, validationGenerator, validationSteps = None, None, None, None
	if args.dataset != "none":
		trainGenerator = datasetReader.iterate("train", miniBatchSize=args.batch_size)
		trainSteps = datasetReader.getNumIterations("train", miniBatchSize=args.batch_size, accountTransforms=True)
		if args.use_validation_set:
			validationGenerator = datasetReader.iterate("validation", miniBatchSize=args.batch_size)
			validationSteps = datasetReader.getNumIterations("validation", miniBatchSize=args.batch_size, \
				accountTransforms=True)

	return datasetReader, trainGenerator, trainSteps, validationGenerator, validationSteps

def loadWeights(model, args, justWeights):
	loadFunc = model.loadWeights if justWeights else model.loadModel
	if args.model == "eigen" and args.eigen_model_part == "coarse":
		loadFunc(args.eigen_coarse_weights_file)
	else:
		loadFunc(args.weights_file)
	print(model.summary())

def main():
	np.seterr("raise")
	args = parseArguments()

	# Model stuff
	imageShape, depthShape = getShapesForModel(args)
	depthShape = (*depthShape, 1) if len(depthShape) == 2 else []
	dIn, dOut = imageShape[2], depthShape[2]
	if args.model == "laina":
		model = ModelLaina(baseModelType="resnet50", upSampleType=args.laina_upsample_method, \
			baseModelPreTrained=args.laina_pretrained_model)
	elif args.model == "eigen":
		coarseOnly = args.eigen_model_part == "coarse"
		model = ModelEigen(coarseOnly=coarseOnly)
	elif args.model == "unet":
		model = ModelUNet(upSampleType="conv_transposed")
	elif args.model == "unet_convdilated":
		model = ModelUNetDilatedConv(dIn=dIn, dOut=dOut, numFilters=args.unet_initial_filters, \
			bottleneckMode=args.unet_bottleneck_mode)
	elif args.model == "unet_convdilated_bugged":
		model = ModelUNetDilatedConvBugged(inputShape=imageShape, numFilters=args.unet_initial_filters)
	model = maybeCuda(model)

	# Dataset stuff
	# transforms = ["none", "mirror"] if args.type in ("train", "retrain") else ["none"]
	transforms = args.data_transforms.split(",")
	validationTransforms = ["none"]
	datasetReader, trainGenerator, trainSteps, validationGenerator, validationSteps = \
		getReader(args, imageShape, depthShape, transforms, validationTransforms)

	print("Type: %s. Batch size: %d. Image shape: %s. Depth shape: %s" % (args.type, args.batch_size, \
		imageShape, depthShape))
	# If we're just testing the dataset reader, don't do anything else
	if args.type == "test_reader":
		test_reader(trainGenerator, trainSteps, args)
		return 0

	# Otherwise, initialize the metrics/optimizers/loss and start training/testing
	changeDirectory(args)
	callbacks = getCallbacks(args, datasetReader)

	# Metrics stuff. For l2_log_loss, values are very small, multiply them by 1000 to see the changes better.
	# Don't add l2_loss if it is already the default loss. Use it for l2_log_loss or berhu.
	metrics = {}
	if args.loss != "l2_loss" and args.loss != "l2_loss_non_zero":
		metrics["L2 Loss"] = lambda x, y, **k : np.mean(np.sum( (x - y)**2, axis=(1, 2)))
	metrics["MSE"] = lambda x, y, **k : np.mean(np.mean( (x - y)**2, axis=(1, 2)))
	metrics["L1 Loss"] = lambda x, y, **k : np.mean(np.sum(np.abs(x - y), axis=(1, 2)))
	model.setMetrics(metrics)

	# Final stuff, set criterion and optimizer based on args.
	setCriterion(args, model)
	setOptimizer(args, model)

	if args.type in ("train", "train_pretrained"):
		# We are loading a pre-trained model, but we don't care about it's history, just using it as a starting point
		if args.type == "train_pretrained":
			model.load_weights(args.weights_file)

		# If training just fine part, we need to have a pre-trained coarse part
		if args.model == "eigen" and args.eigen_model_part == "fine":
			model.coarse.load_weights(args.eigen_coarse_weights_file)
			# Freeze the parameters of the coarse model
			for param in model.coarse.parameters():
				param.requires_grad = False
		print(model.summary())
		model.train()
		model.train_generator(trainGenerator, stepsPerEpoch=trainSteps, numEpochs=args.num_epochs, \
			callbacks=callbacks, validationGenerator=validationGenerator, validationSteps=validationSteps)
	elif args.type == "retrain":
		# Use load_model, which also loads the optimizer state, alongside with the weights
		loadWeights(model, args, justWeights=False)
		model.train()
		model.train_generator(trainGenerator, stepsPerEpoch=trainSteps, numEpochs=args.num_epochs, \
			callbacks=callbacks, validationGenerator=validationGenerator, validationSteps=validationSteps)
	elif args.type == "test":
		loadWeights(model, args, justWeights=True)
		test(args, model, datasetReader)
	elif args.type == "add_validation":
		addValidation(model, datasetReader, args.add_validation_models_dir, args.batch_size, \
			args.add_validation_model_start)
	elif args.type == "inference":
		loadWeights(model, args, justWeights=True)
		inference(args, model, imageShape)

		Sum = timedelta(0)
		for i in range(15):
			now = datetime.now()
			inference(args, model, imageShape)
			took = datetime.now() - now
			Sum += took
			print("Took: %s" % (took))
		print("Mean: %s" % (Sum / 15))
	elif args.type == "inference_video":
		loadWeights(model, args, justWeights=True)
		exportVideo(model, args)

if __name__ == "__main__":
	main()
