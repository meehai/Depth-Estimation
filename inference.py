import sys
import torch as tr
import numpy as np
from plotter import paramsHandler, nicePlotter, getInputMetadata
from lycon import resize, Interpolation
from neural_wrappers.pytorch import maybeCuda
from neural_wrappers.utilities import standardizeData, minMaxNormalizeData
from neural_wrappers.models import ModelLaina, ModelUNetDilatedConv
from model_unet_convdilated_bugged import ModelUNetDilatedConvBugged
from PIL import Image

baseModel = None

def prepareBaseModel(args):
	global baseModel
	if not baseModel is None:
		return

	assert args.inference_prev_depth_mode in ("regular", "deeplabv3_new_dims", "deeplabv3_foreground_background")
	numDimensions = 3 if args.inference_prev_depth_mode == "regular" else 22
	inH, inW = map(lambda x : int(x), args.inference_input_shape.split(","))
	assert args.inference_base_model in ("laina", "unet_convdilated", "unet_convdilated_bugged")
	if args.inference_base_model == "laina":
		# assert inH == 228 and inW == 304
		baseModel = ModelLaina(baseModelType="resnet50", upSampleType=args.laina_upsample_method, \
			baseModelPreTrained=True)
	elif args.inference_base_model == "unet_convdilated":
		baseModel = ModelUNetDilatedConv(inputShape=(inH, inW, numDimensions), numFilters=64)
	elif args.inference_base_model == "unet_convdilated_bugged":
		baseModel = ModelUNetDilatedConvBugged(inputShape=(inH, inW, numDimensions), numFilters=64)
	baseModel.load_weights(args.inference_base_model_weights)
	baseModel = maybeCuda(baseModel)
	return baseModel

def normalize(data, normalizationType):
	numDims = data.shape[-1]
	data = np.float32(data)
	if normalizationType == "min_max_normalization":
		for i in range(numDims):
			data[..., i] = minMaxNormalizeData(data[..., i], np.min(data[..., i]), np.max(data[..., i]))
	elif normalizationType == "standardization":
		for i in range(numDims):
			data[..., i] = standardizeData(data[..., i], np.mean(data[..., i]), np.std(data[..., i]))
	return data

def doPng(imagePath, imageShape):
	height, width = imageShape[0 : 2]
	image = np.uint8(Image.open(imagePath))
	image = resize(image, height=height, width=width, interpolation=Interpolation.LANCZOS)
	image = np.expand_dims(image, axis=0)
	return image

def semanticFGBG(images):
	newImage = np.ones(images.shape, dtype=np.float32)
	labels = {
		"sky" : np.where(images == 23),
		"buildings" : np.where(images == 11),
		"logo" : np.where(images == 1),
		"road" : np.where(images == 7),
		"sidewalk" : np.where(images == 22),
		"sidewalk2": np.where(images == 8),
		"trees" : np.where(images == 21)
	}

	for key in labels:
		newImage[labels[key]] = 0
	return newImage

def semanticNewDims(images):
	importantIds = [7, 8, 11, 12, 13, 17, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 31, 32, 33]
	newImages = np.zeros((*images.shape, 19), dtype=np.float32)
	for i in range(len(importantIds)):
		whereId = np.where(images == importantIds[i])
		newImages[..., i][whereId] = 1
	return newImages

def doSemanticDeepLabV3(pngImage, semanticTransform, imageShape):
	sys.path.append("/home/mihai/Public/semantic-segmentation/models/research/deeplab/")
	from deeplabv3_inference import runImage as deepLabV3RunImage

	height, width = imageShape[0 : 2]
	semanticImage = deepLabV3RunImage(pngImage, height=height, width=width)
	semanticImage = np.expand_dims(semanticImage, axis=0)

	if semanticTransform == "default":
		semanticImage = np.float32(semanticImage) / 33
		semanticImage = np.expand_dims(semanticImage, axis=-1)
	elif semanticTransform == "foreground-background":
		semanticImage = semanticFGBG(semanticImage)
		semanticImage = np.expand_dims(semanticImage, axis=-1)
	elif semanticTransform == "semantic_new_dims":
		semanticImage = semanticNewDims(semanticImage)
	else:
		assert False

	return semanticImage

def doSemantic(pngImage, semanticType, semanticTransform, imageShape):
	assert semanticType in ("deeplabv3", )

	if semanticType == "deeplabv3":
		semanticImage = doSemanticDeepLabV3(pngImage, semanticTransform, imageShape)

	return semanticImage

def doFlowNet2S(pngImage, pngPrevImage, opticalFlowTransform, imageShape):
	sys.path.append("/home/mihai/Public/optical-flow/flownet2-pytorch")
	from flownet2s_inference import runImages as flownet2SRunImages

	height, width = imageShape[0 : 2]
	prevImage = resize(pngPrevImage, height=1024, width=2048, interpolation=Interpolation.LANCZOS)
	currentImage = resize(pngImage, height=1024, width=2048, interpolation=Interpolation.LANCZOS)

	flow_y, flow_x = flownet2SRunImages(prevImage, currentImage)
	flow_y = resize(flow_y, height=height, width=width, interpolation=Interpolation.LANCZOS)
	flow_x = resize(flow_x, height=height, width=width, interpolation=Interpolation.LANCZOS)

	flow_y = flow_y.reshape((1, height, width, 1))
	flow_x = flow_x.reshape((1, height, width, 1))

	if opticalFlowTransform == "magnitude":
		flow = np.hypot(flow_y, flow_x)
	elif opticalFlowTransform == "none":
		flow = np.concatenate([flow_y, flow_x], axis=-1)
	else:
		assert False
	return flow

def doOpticalFlow(pngImage, pngPrevImage, opticalFlowType, opticalFlowTransform, imageShape):
	assert opticalFlowType in ("flownet2s", )

	if opticalFlowType == "flownet2s":
		flow = doFlowNet2S(pngImage, pngPrevImage, opticalFlowTransform, imageShape)

	return flow

# TODO: for continuous inference mode (if ever?), find a way to use previous depth, from time T-1
def doPrevDepth(imagePath, prevDepthMode, imageShape, normalizationType):
	global baseModel
	prevImage = doPng(imagePath, imageShape)
	normPrevImage = normalize(prevImage, normalizationType)

	if prevDepthMode == "deeplabv3_new_dims":
		semanticImage = doSemantic(prevImage[0], "deeplabv3", "semantic_new_dims", imageShape)
		normPrevImage = np.concatenate([normPrevImage, semanticImage], axis=-1)
	elif prevDepthMode == "deeplabv3_foreground_background":
		semanticImage = doSemantic(prevImage[0], "deeplabv3", "foreground-background", imageShape)
		normPrevImage = np.concatenate([normPrevImage, semanticImage], axis=-1)

	trInput = maybeCuda(tr.from_numpy(normPrevImage))
	trResult = baseModel.forward(trInput)
	npResult = trResult.cpu().numpy()
	return np.expand_dims(npResult, axis=-1)

# images is dictionary with keys "currentImage", "prevImage", "prevDepth" or any other that might come
def doInference(args, model, images, imageShape, numChannels, dimensionsIndexes):
	inH, inW = imageShape[0 : 2]
	inputDims = np.zeros((inH, inW, numChannels), dtype=np.float32)
	inputDims[..., 0 : 3] = images["currentImage"]

	if args.inference_rgb_prev_frame:
		assert "prevImage" in images
		l, r = dimensionsIndexes["rgb_prev_frame"]
		inputDims[..., l : r] = images["prevImage"]

	if args.inference_rgb_older_frame:
		assert "firstImage" in images
		l, r = dimensionsIndexes["rgb_first_frame"]
		inputDims[..., l : r] = images["firstImage"]

	if args.inference_prev_depth:
		assert "prevDepth" in images
		l, r = dimensionsIndexes["depth_prev_frame_" + args.inference_prev_depth_mode]
		inputDims[..., l : r] = images["prevDepth"]

	if not args.inference_semantic is None:
		imageSemantic = doSemantic(images["currentImage"], args.inference_semantic, \
			args.inference_semantic_transform, imageShape)
		l, r = dimensionsIndexes[args.inference_semantic]
		inputDims[..., l : r] = imageSemantic

	if not args.inference_optical_flow is None:
		assert not args.inference_rgb_prev_frame_path is None
		# previousImage = np.uint8(Image.open(args.inference_rgb_prev_frame_path))
		flow = doOpticalFlow(images["currentImage"], images["prevImage"], args.inference_optical_flow, \
			args.inference_optical_flow_transform, imageShape)
		flow = normalize(flow, args.data_normalization)
		l, r = dimensionsIndexes[args.inference_optical_flow]
		inputDims[..., l : r] = flow

	if not args.inference_hvn is None:
		assert False, "HVN inference - Not yet supported"

	npInput = np.expand_dims(inputDims, axis=0)
	trInput = maybeCuda(tr.from_numpy(npInput))
	trResult = model.forward(trInput).cpu().detach()
	npResult = trResult.numpy()[0]
	return npInput, npResult

def inference(args, model, imageShape):
	import matplotlib.pyplot as plt
	numChannels, _, dimensionsIndexes = getInputMetadata(args)

	originalImage = np.uint8(Image.open(args.inference_rgb))
	image = normalize(doPng(args.inference_rgb, imageShape), args.data_normalization)
	images = {
		"currentImage" : normalize(doPng(args.inference_rgb, imageShape), args.data_normalization),
	}

	if args.inference_rgb_prev_frame:
		assert not args.inference_rgb_prev_frame_path is None
		images["prevImage"] = normalize(doPng(args.inference_rgb_prev_frame_path, imageShape), args.data_normalization)

	if args.inference_rgb_older_frame:
		assert not args.inference_rgb_older_frame is None
		images["firstImage"] = normalize(doPng(args.inference_rgb_older_frame, imageShape), args.data_normalization)

	if args.inference_prev_depth:
		assert not (args.inference_rgb_prev_frame_path is None and args.inference_prev_depth is None)
		if not args.inference_rgb_prev_frame_path:
			args.inference_rgb_prev_frame_path = args.inference_prev_depth

		prepareBaseModel(args)
		images["prevDepth"] = doPrevDepth(args.inference_rgb_prev_frame_path, args.inference_prev_depth_mode, \
			imageShape, args.data_normalization)

	npInput, npResult = doInference(args, model, images, imageShape, numChannels, dimensionsIndexes)

	if args.inference_plot_result:
		images, titles, params = paramsHandler(npInput[0], None, npResult, args)
		nicePlotter(images=images, titles=titles, params=params)
		plt.show(block=True)
	return npResult