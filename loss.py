import torch as tr

Type = tr.FloatTensor if not tr.cuda.is_available() else tr.cuda.FloatTensor

def l2_log_loss(networkOutput, label, Lambda=0.5):
	logOutput = tr.log(tr.clamp(networkOutput, 1e-4, float("inf")) + 1.0)
	logLabel = tr.log(tr.clamp(label, 1e-4, float("inf")) + 1.0)
	res1 = tr.sum( (logOutput - logLabel)**2, dim=1).sum(dim=1)
	res2 = tr.mean(logOutput - logLabel, dim=1).mean(dim=1)**2
	res = res1 - Lambda * res2
	return tr.mean(res)

def l2_loss(networkOutput, label, Lambda=0):
	res1 = tr.sum( (networkOutput - label)**2, dim=1).sum(dim=1)
	#res2 = tr.mean(networkOutput - label, dim=1)**2
	#res = res1 - Lambda * res2
	return tr.mean(res1)

def l1_loss(networkOutput, label, Lambda=0):
	res1 = tr.sum(tr.abs(networkOutput - label), dim=1).sum(dim=1)
	#res2 = tr.mean(networkOutput - label, dim=1)**2
	#res = res1 - Lambda * res2
	return tr.mean(res1)

def l2_loss_non_zero(networkOutput, label):
	y = (networkOutput - label)**2
	y2 = (label != 0).type(Type)
	res = tr.sum(y * y2, dim=1).sum(dim=1)
	return tr.mean(res)

# TODO
def hubber_loss(networkOutput, label):
	pass
