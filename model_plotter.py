import sys
import torch as tr
import matplotlib.pyplot as plt
import numpy as np

def doPlot(model, name, maxValue, show):
	modelHistory = model["history_dict"]
	trainLoss = np.array([i["trainMetrics"]["Loss"] for i in modelHistory])
	valLoss = np.array([i["validationMetrics"]["Loss"] for i in modelHistory])

	minX, minValue = np.argmin(valLoss), np.min(valLoss)

	x = np.arange(len(trainLoss)) + 1
	plt.plot(x, trainLoss, label="Train Loss")
	plt.plot(x, valLoss, label="Validation Loss")
	plt.xlabel("Epoch")
	plt.ylabel("Loss")
	x1, x2, y1, y2 = plt.axis()
	plt.axis((x1, x2, 0, maxValue))
	plt.legend()

	offset = minValue // 2
	plt.annotate("Epoch %d\nMax %2.2f" % (minX + 1, minValue), xy=(minX + 1, minValue - offset))
	plt.plot([minX + 1], [minValue], "o")

	plt.savefig(name, dpi=120)
	if show:
		plt.show()

def main():
	assert len(sys.argv) == 5, "Usage: python model_plotter.py model_path.pkl result.png maxValue show=0/1"
	assert sys.argv[4] in ("0", "1")
	model = tr.load(sys.argv[1])
	name = sys.argv[2]
	maxValue = int(sys.argv[3])
	show = int(sys.argv[4])
	doPlot(model=model, name=name, maxValue=maxValue, show=show)

if __name__ == "__main__":
	main()